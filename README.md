# title

## Voxpupuli - an Open Source Speedtrain

# Proposed Session Description (max 125 words)

Vox Pupuli, the Voice of the Puppets, is an Open Source Community centred around Puppet and its ecosystem. The most important idea is to have more than one maintainer. The most powerful effect from that has been a flourishing community moving forward at a pace that our colleagues in the Puppetlabs Module team envy.

In this session I'd like to talk about how we've accomplished that, how *you* can *participate* in and *learn* from our practices.

# Your Session Audience Level

Intermediate

# What do you want the audience to gain from your session?    

After this talk, the audience should know what Voxpupuli is, and why *they* should want to contribute to this open source effort. They will be familiar with open source best practices, many of which can be applied in the enterprise.

## Bio

Igor is a freelancer and a seasoned Open Source contributor, who usually works on the ops-side of the dev-ops spectrum. With only three years puppet experience, they're *practically* a new-comer.

## LICENSE

<p xmlns:dct="http://purl.org/dc/terms/">
  <a rel="license"
     href="https://creativecommons.org/publicdomain/zero/1.0/">
    <img src="https://i.creativecommons.org/p/zero/1.0/88x31.png" style="border-style: none;" alt="CC0" />
  </a>
  <br />
  To the extent possible under law,
  <span rel="dct:publisher" resource="[_:publisher]">the person who associated CC0</span>
  with this work has waived all copyright and related or neighboring
  rights to this work.
</p>
